var General = function () {
	var initDatatable = function(){
		var init = {};

		$('.dataTable').each(function () {
			init["order"] = [];
			if ($(this).hasClass('no-sort')) init["bSort"] = false;
			if ($(this).hasClass('scroll-horizontal')) init["scrollX"] = true;
		    //if ($(this).hasClass('no-search')) init["bFilter"] = false;
            
			var search = "<'col-md-6'f>";

			if ($(this).hasClass('no-search')) search = "";

			init["dom"] = "<'row'<'col-md-6'l>"+search+">" +
"<'table-scrollable'tr>" +
"<'row'<'col-md-5'i><'col-md-7'p>>";

			//console.log($(this));
            var sortClass = "sort-";
            if ($(this).is('[class*=' + sortClass + ']'))
            {
                var className = $(this).attr("class");

                var cls = $.map(className.split(' '), function (val, i) {
                    if (val.indexOf(sortClass) > -1) {
                        return val.slice(sortClass.length, val.length)
                    }
                });

                cls = cls.join("");
                var col = cls.split('-')[0];
                var dir = cls.split('-')[1];

                init["order"] = [[col, dir]];
            }

            init["pagingType"] = "bootstrap_full_number";
		  
		    init["orderCellsTop"] = true;
		    //init["fixedHeader"] = true;

			// init["responsive"] = true;
			// init["columnDefs"] = [
   //              {  // set default column settings
   //                  'orderable': false,
   //                  'targets': [0]
   //              }, 
   //              {
   //                  "searchable": false,
   //                  "targets": [0]
   //              },
   //              {
   //                  "className": "dt-right", 
   //                  //"targets": [2]
   //              }
   //          ];

			if (!$.fn.dataTable.isDataTable(this)) {
				try {
					table = $(this).DataTable(
						init
						);
				} catch (e) {
					alert(e);
				}
			}
		});
	};

	var initActiveLink = function(){
		var path = location.href;
        $('.nav-item > a[href="' + path + '"]').closest('li').addClass('active');
	};

	var initSummernote = function(){
		$('.summerNote').each(function () { 
			$(this).summernote({height: 300});
		});
	};

	var initDatepicker = function(){
	    if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
		}
	};

	var initTimepicker = function() {
        $('.timepicker').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: false,
            showMeridian: false,
            defaultTime: 'value'
        });
	};

	var initBootstrapSelect = function(){
		$('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
	};

	var initFileUpload = function () {
	    $(document).on('change', '.btn-file :file', function () {
	        var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	        input.trigger('fileselect', [numFiles, label]);
	    });

	    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {
	        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

	        if (input.length) {
	            input.val(log);
	        } else {
	            if (log) alert(log);
	        }
	    });
	};

	var initSelect2 = function () {
	    $.fn.select2.defaults.set("theme", "bootstrap");

	    $('.select2').each(function () {
	        var init = {};

	        var first_opt = $(this).find("option:first");
	        var placeholder = $(first_opt).val() == "" ? $(first_opt).text() : "- Please select a value -";

	        if (!$(this).hasClass("no-placeholder")) { init["placeholder"] = placeholder; }
	        if ($(this).hasClass("no-search")) { init["minimumResultsForSearch"] = Infinity; }
	        if ($(this).attr("multiple")) { init["closeOnSelect"] = false; }

	        init["width"] = null;
	        init["templateResult"] = formatDesign;
	        init["templateSelection"] = formatDesign;

	        if ($(this).data('select2')) {
	            $(this).select2("destroy");
	            //console.log("select2");
	        }
            
	        try {
	            $(this).select2(
					init
				);
	        } catch (e) {
	            alert(e);
	        }

	        if ($(this).hasClass("input-sm")) { $(this).next(".select2-container--bootstrap").addClass("input-sm"); }
	    });

	    function formatDesign(item) {
	        //console.log("asdf");
	        if (item.text.indexOf("<br>") > -1) {
	            var selectionText = item.text.split("<br>");
	            var $returnString = $("<span>" + item.text + "</span>");
	            return $returnString;
	        }
	        else if (item.title == "2") {
	            return $("<span class='font-red-pink'>" + item.text + "</span>");
	        }
	        else if (item.text.indexOf("#") > -1) {
	            var selectionText = item.text.split("#");
	            var $returnString = $('<span>' + selectionText[0] + '</br><small>' + selectionText[1] + '</small></span>');
	            return $returnString;
	        } else {
	            return item.text;
	        }
	    }
	}

	var initRepeater = function () {
	    
	};

	return{
		init: function() {		
			initDatatable();
			//initSummernote();
			initDatepicker();
			//initTimepicker();
			//initBootstrapSelect();
			initActiveLink();
			initFileUpload();
			initSelect2();
			initRepeater();
		},

		reinitSelect2: function (){
		    initSelect2();
		},

		thousandSeparator: function (x) {
		    var separator = ",";
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
		},

		removeThousandSeprator: function (x) {
		    //,(?=[\d,]*\.\d{2}\b)
		    return x.toString().replace(/,/g, '');
		},

		alert_delete: function(obj){
			swal({
			  	title: "Are you sure?",
			  	text: "Your will not be able to recover this data!",
			  	type: "warning",
		  		showCancelButton: true,
			  	confirmButtonClass: "btn-danger",
			  	confirmButtonText: "Yes, delete it!"
			},
			function(){
				location.href = $(obj).attr("href");
			});
		},

		alert_disable: function(obj){
			swal({
			  	title: "Are you sure?",
			  	text: "Selected user will be not able to access Intelli Outsource!",
			  	type: "warning",
		  		showCancelButton: true,
			  	confirmButtonClass: "btn-danger",
			  	confirmButtonText: "Yes, disable it!"
			},
			function(){
				location.href = $(obj).attr("href");
			});
		},

		alert_interview: function(obj){
			swal({
			  	title: "Are you sure?",
			  	text: "Your will create iterview schedule",
			  	type: "info",
		  		showCancelButton: true,
			  	confirmButtonClass: "btn-success",
			  	confirmButtonText: "Yes, assign it!"
			},
			function(){
				return true;
			});	
		},

		alert_assign: function(obj, comp_name, job_name){
			var tr = $(obj).parents("tr");
			var td = $(tr).find("td").eq(0);
			var name = td.data("candidate-name");

			swal({
			  	title: "Are you sure?",
			  	text: "Your will assign "+name+" to "+comp_name+" as " + job_name,
			  	type: "info",
		  		showCancelButton: true,
			  	confirmButtonClass: "btn-success",
			  	confirmButtonText: "Yes, assign it!"
			},
			function(){
				location.href = $(obj).attr("href");
			});	
		},

		getDate: function(){
		    var today = new Date();
		    var dd = today.getDate();
		    var mm = today.getMonth() + 1; //January is 0!

		    var yyyy = today.getFullYear();
		    if (dd < 10) {
		        dd = '0' + dd;
		    }
		    if (mm < 10) {
		        mm = '0' + mm;
		    }
		    var today = yyyy + "" + mm + "" + dd;
		    return today;
		},

		notification: function(options) {
			ops = $.extend(true, {
                container: "",
                type: 'success',
                message: "",
                autoclose: false,
                icon: ""

                
            }, options);

			var type = ops.type;
			var closeInSeconds = ops.autoclose ? 5 : 0;
			var icon = ops.type == "success" ? "check" : ops.type;

            App.alert({
                container: ops.container,
                type: type,
                message: ops.message,
                closeInSeconds: closeInSeconds, 
                icon: icon
            });
		}
	}
}();

jQuery(document).ready(function() {    
    General.init();
});
