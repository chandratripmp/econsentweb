﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
    public static class HtmlExtensions
    {
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString LabelRequiredFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string labelText = "", object htmlAttributes = null)
        {
            return LabelHelper(html,
                ModelMetadata.FromLambdaExpression(expression, html.ViewData),
                ExpressionHelper.GetExpressionText(expression), labelText, new RouteValueDictionary(htmlAttributes));
        }

        private static MvcHtmlString LabelHelper(HtmlHelper html,
            ModelMetadata metadata, string htmlFieldName, string labelText, IDictionary<string, object> htmlAttributes)
        {
            if (string.IsNullOrEmpty(labelText))
            {
                labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            }

            if (string.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            bool isRequired = metadata.IsRequired;

            //if (metadata.ContainerType != null)
            //{
            //    isRequired = metadata.ContainerType.GetProperty(metadata.PropertyName)
            //                    .GetCustomAttributes(typeof(RequiredAttribute), false)
            //                    .Length == 1;
            //}

            TagBuilder tag = new TagBuilder("label");
            tag.Attributes.Add(
                "for",
                TagBuilder.CreateSanitizedId(
                    html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)
                )
            );

            tag.MergeAttributes(htmlAttributes);

            tag.SetInnerText(labelText);

            if (isRequired)
            {
                var asteriskTag = new TagBuilder("span");
                asteriskTag.Attributes.Add("class", "text-danger");
                asteriskTag.SetInnerText(" *");
                //output += asteriskTag.ToString(TagRenderMode.Normal);
                tag.InnerHtml += asteriskTag.ToString(TagRenderMode.Normal);
            }

            var output = tag.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(output);
        }
    }
}