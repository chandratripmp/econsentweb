﻿using eConsentWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq.Dynamic;
using System.Net;
using System.Linq.Expressions;
using eConsentWeb.Common;

namespace eConsentWeb.Controllers
{
    public class ConsentController : Controller
    {
        private eConsentEntities db = new eConsentEntities();

        const string s_OptOut = "Opted Out";
        const string s_OptIn = "Opted In";

        public ActionResult Index()
        {
            ViewBag.Speciality = GenerateDDLSpeciality();
            ViewBag.Province = GenerateDDLProvince();
            ViewBag.City = GenerateDDLCity();

            return View();
        }

        // GET: Consent/Details/5
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ConsentDetailsViewModel CDVM = new ConsentDetailsViewModel();
            
            CDVM.MSConsent = db.MS_eConsent.Where(w => w.doctorCode == id).FirstOrDefault();

            if (CDVM.MSConsent == null)
            {
                return HttpNotFound();
            }

            CDVM.HCPProfile = db.MS_eConsent_HCPProfile.Where(w => w.doctorCode == id).FirstOrDefault();
            CDVM.TRConsentList = db.TR_eConsent.Where(w => w.doctorCode == id).OrderByDescending(o => o.consentDate).ToList();

            return View(CDVM);
        }

        public void Export()
        {
            string filename = "[eConsent] Consent List Report_" + DateTime.Now.ToString("yyyyMMdd");

            if (((UserRole)Session["UserRole"]) == UserRole.Admin)
            {
                List<ConsentAdminExportModels> vAdmin = (
                    from m in db.MS_eConsent
                    join p in db.MS_eConsent_HCPProfile on m.doctorCode equals p.doctorCode into JoinedConsent
                    from p in JoinedConsent.DefaultIfEmpty()
                    join t in db.TR_eConsent on m.doctorCode equals t.doctorCode into JoinedTrxConsent
                    from t in JoinedTrxConsent.DefaultIfEmpty()
                    select new { m, p, t }).Select(s => new ConsentAdminExportModels
                    {
                        DOCTOR_CODE     = "=\"" + s.m.doctorCode + "\"",
                        NAME            = s.m.doctor,
                        SPECIALITY      = s.m.specialty,
                        CONSENT_LEVEL   = s.m.consentLevel,

                        STR             = s.p != null ? s.p.str                           : " - ",
                        GENDER          = s.p != null ? s.p.gender                        : " - ",
                        KOL_STATUS      = s.p != null ? s.p.KOLstatus                     : " - ",
                        PROVINCE        = s.p != null ? s.p.province                      : " - ",
                        CITY            = s.p != null ? s.p.city                          : " - ",
                        MAIN_PRACTICE   = s.p != null ? s.p.mainPractice                  : " - ",
                        PRACTICE1       = s.p != null ? s.p.practice1                     : " - ",
                        PRACTICE2       = s.p != null ? s.p.practice2                     : " - ",
                        MLD_STATUS      = s.p != null ? s.p.MLDstatus                     : " - ",
                        CONSENT_STATUS  = s.p != null ? s.p.consentStatus                 : " - ",

                        SERIES_ID       = s.t != null ? "=\"" + s.t.seriesID + "\""       : " - ",
                        CONSENT_CHANNEL = s.t != null ? s.t.consentChannels               : " - ",
                        CONSENT_DETAIL  = s.t != null ? "=\"" + s.t.consentDetails + "\"" : " - ",
                        OPT_STATUS      = s.t != null ? s.t.optStatus                     : " - ",
                        CONSENT_DATE    = s.t.consentDate ?? s.t.consentDate.Value
                    }
                ).ToList();

                ExportToFile(vAdmin, filename);
            }
            else
            {
                List<ConsentUserExportModels> vUser = (
                    from m in db.MS_eConsent
                    join p in db.MS_eConsent_HCPProfile on m.doctorCode equals p.doctorCode into JoinedConsent
                    from p in JoinedConsent.DefaultIfEmpty()
                    select new { m, p }).Select(s => new ConsentUserExportModels
                    {
                        DOCTOR_CODE    = "=\"" + s.m.doctorCode + "\"",
                        NAME           = s.m.doctor,
                        SPECIALITY     = s.m.specialty,
                        CONSENT_LEVEL  = s.m.consentLevel,

                        STR            = s.p != null ? s.p.str           : " - ",
                        GENDER         = s.p != null ? s.p.gender        : " - ",
                        KOL_STATUS     = s.p != null ? s.p.KOLstatus     : " - ",
                        PROVINCE       = s.p != null ? s.p.province      : " - ",
                        CITY           = s.p != null ? s.p.city          : " - ",
                        MAIN_PRACTICE  = s.p != null ? s.p.mainPractice  : " - ",
                        PRACTICE1      = s.p != null ? s.p.practice1     : " - ",
                        PRACTICE2      = s.p != null ? s.p.practice2     : " - ",
                        MLD_STATUS     = s.p != null ? s.p.MLDstatus     : " - ",
                        CONSENT_STATUS = s.p != null ? s.p.consentStatus : " - "
                    }
                ).ToList();

                ExportToFile(vUser, filename);
            }
        }

        private void ExportToFile(object data, string filename)
        {
            var gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }

        public ActionResult ViewFile(string id)
        {
            TR_eConsent tr = db.TR_eConsent.Where(w => w.consentID == id).FirstOrDefault();

            if (tr == null)
            {
                return HttpNotFound();
            }

            MS_eConsent ms = db.MS_eConsent.Where(w => w.doctorCode == tr.doctorCode).FirstOrDefault();

            var filename = tr.pdfFileName;
            var sharedFolder = System.Configuration.ConfigurationManager.AppSettings["pdfFolder"];
            var doctorFolder = tr.doctorCode + "_" + ms.doctor;
            var path = sharedFolder + "\\" + doctorFolder + "\\" + filename;

            if (!System.IO.File.Exists(path))
            {
                return HttpNotFound();
            }
            
            return File(path, "application/pdf");
        }

        public SelectList GenerateDDLSpeciality()
        {
            var specialities = db.MS_eConsent.AsNoTracking().OrderBy(o => o.specialty).Select(s => s.specialty).Distinct().ToList();

            List<SelectListItem> ddlSpeciality = specialities
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n,
                            Text = n
                        }).ToList();

            return new SelectList(ddlSpeciality, "Value", "Text");
        }

        public SelectList GenerateDDLProvince()
        {
            var provinces = db.MS_eConsent_HCPProfile.AsNoTracking().OrderBy(o => o.province).Select(s => s.province).Distinct().ToList();

            List<SelectListItem> ddlProvince = provinces
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n,
                            Text = n
                        }).ToList();

            return new SelectList(ddlProvince, "Value", "Text");
        }

        public SelectList GenerateDDLCity()
        {
            var cities = db.MS_eConsent_HCPProfile.AsNoTracking().OrderBy(o => o.city).Select(s => s.city).Distinct().ToList();

            List<SelectListItem> ddlCity = cities
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n,
                            Text = n
                        }).ToList();

            return new SelectList(ddlCity, "Value", "Text");
        }

        [HttpPost]
        public ActionResult LoadConsent()
        {
            // datatables.js filter
            var draw           = Request.Form.GetValues("draw").FirstOrDefault();
            var start          = Request.Form.GetValues("start").FirstOrDefault();
            var length         = Request.Form.GetValues("length").FirstOrDefault();

            // custom filter
            var optStatusIn    = Request.Form.GetValues("opt_status_in").FirstOrDefault();
            var optStatusOut   = Request.Form.GetValues("opt_status_out").FirstOrDefault();
            var speciality     = Request.Form.GetValues("speciality[]");
            var province       = Request.Form.GetValues("province[]");
            var city           = Request.Form.GetValues("city[]");
            var consentChannel = Request.Form.GetValues("consent_channel[]");

            //Find Order Column
            var sortColumn     = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir  = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //Find Search Keyword
            var searchKey      = Request.Form.GetValues("search[value]").FirstOrDefault().ToLower();

            //Find Individual Search Keyword
            //var indSearchKey = Request.Form.GetValues("columns").ToList();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;


            List<ConsentIndexViewModel> v = (
                from m in db.MS_eConsent
                join p in db.MS_eConsent_HCPProfile
                    on m.doctorCode equals p.doctorCode into JoinedConsent
                from p in JoinedConsent.DefaultIfEmpty()
                join t in db.TR_eConsent
                    on m.doctorCode equals t.doctorCode into JoinedLatestTrxConsent
                from t in JoinedLatestTrxConsent.DefaultIfEmpty()


                join pvt in db.TR_eConsent.GroupBy(g => g.doctorCode)
                    .Select(s => new TR_eConsentPivot
                    {
                        doctorCode = s.Key,
                        phoneNumberCount = s.Count(c => c.consentChannels == "Nomor Ponsel"),
                        emailCount = s.Count(c => c.consentChannels == "Email")
                    })
                    on m.doctorCode equals pvt.doctorCode


                where t.id == (from t2 in db.TR_eConsent where t2.doctorCode == t.doctorCode orderby t2.consentDate descending select t2.id).FirstOrDefault()
                select new { m, p, t, pvt }).AsEnumerable().Select(s => new ConsentIndexViewModel
                {
                    DOCTOR_CODE = s.m.doctorCode,
                    NAME = s.m.doctor,
                    SPECIALITY = s.m.specialty,
                    GENDER = s.p != null ? s.p.gender : " - ",
                    SPECIALITY1 = s.p != null ? s.p.speciality1 : " - ",
                    SPECIALITY2 = s.p != null ? s.p.speciality2 : " - ",
                    STR = s.p != null ? s.p.str : " - ",
                    CITY = s.p != null ? s.p.city : " - ",
                    PROVINCE = s.p != null ? s.p.province : " - ",
                    CONSENT_STATUS = s.p != null ? s.p.consentStatus : " - ",

                    TITLE_ID = s.p != null ? s.p.titleID : " - ",
                    KOL_STATUS = s.p != null ? s.p.KOLstatus : " - ",
                    MAIN_PRACTICE = s.p != null ? s.p.mainPractice : " - ",
                    PRACTICE1 = s.p != null ? s.p.practice1 : " - ",
                    PRACTICE2 = s.p != null ? s.p.practice2 : " - ",
                    MLD_STATUS = s.p != null ? s.p.MLDstatus : " - ",

                    OPT_STATUS = s.t != null ? s.t.optStatus : " - ",

                    CONSENT_CHANNEL = ((s.pvt.phoneNumberCount > 0 ? "Nomor Ponsel, " : "") + (s.pvt.emailCount > 0 ? "Email" : "")).TrimEnd(',').Trim(),
                    CONSENT_CHANNEL_ARR = (s.pvt.phoneNumberCount > 0 && s.pvt.emailCount > 0 ? new string[] { "Nomor Ponsel", "Email" } : ((s.pvt.phoneNumberCount > 0 ? new string[] { "Nomor Ponsel" } : ((s.pvt.emailCount > 0 ? new string[] { "Email" } : new string[] { } )))))
                }).ToList();
            
            if (optStatusIn != optStatusOut)
            {
                if (optStatusIn == "true")
                    v = v.Where(w => w.OPT_STATUS == s_OptIn).ToList();
                else
                    v = v.Where(w => w.OPT_STATUS == s_OptOut).ToList();
            }

            if (speciality != null)
            {
                //v = v.Where(w => w.SPECIALITY1 == speciality || w.SPECIALITY2 == speciality).ToList();
                v = v.Where(w => speciality.Contains(w.SPECIALITY) || speciality.Contains(w.SPECIALITY1) || speciality.Contains(w.SPECIALITY2)).ToList();
            }

            if (province != null)
            {
                //v = v.Where(w => w.PROVINCE == province).ToList();
                v = v.Where(w => province.Contains(w.PROVINCE)).ToList();
            }

            if (city != null)
            {
                //v = v.Where(w => w.CITY == city).ToList();
                v = v.Where(w => city.Contains(w.CITY)).ToList();
            }

            if (consentChannel != null)
            {
                //v = v.Where(w => w.CONSENT_CHANNEL.Contains(consentChannel)).ToList();
                v = v.Where(w => consentChannel.Intersect(w.CONSENT_CHANNEL_ARR).Any()).ToList();
            }

            //FILTER
            if (!string.IsNullOrEmpty(searchKey))
            {
                List<string> wheres = new List<string>();

                foreach (string key in Request.Form)
                {
                    if (key.EndsWith("[name]") && !string.IsNullOrEmpty(Request.Form.GetValues(key).FirstOrDefault()))
                    {
                        string col = key.Substring(0, key.IndexOf("[name]"));
                        string colName = Request.Form.GetValues(col + "[name]").FirstOrDefault();
                        bool searchable = Convert.ToBoolean(Request.Form.GetValues(col + "[searchable]").FirstOrDefault());

                        if (searchable)
                        {
                            var where = "(" + colName + " != null && " + colName + ".ToLower().Contains(\""+searchKey+"\"))";
                            wheres.Add(where);

                            //wheres.Add(colName);
                        }
                    }
                }

                string whereQ = String.Join(" || ", wheres.ToArray());
                v = v.Where(whereQ).ToList();
            }

            //DATE RANGE FILTER
            //if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            //{
            //v = v.Where("D_CREATEDDATE >= @0 && D_CREATEDDATE <= @1", Convert.ToDateTime(from + " 00:00"), Convert.ToDateTime(to + " 23:59")).ToList();
            //}

            //INDIVIDUAL SEARCH
            //foreach (string key in Request.Form)
            //{
            //    if (key.EndsWith("[search][value]") && !string.IsNullOrEmpty(Request.Form.GetValues(key).FirstOrDefault()))
            //    {
            //        string col = key.Substring(0, key.IndexOf("[search][value]"));
            //        string value = Request.Form.GetValues(col + "[search][value]").FirstOrDefault().ToLower();
            //        string colName = Request.Form.GetValues(col + "[name]").FirstOrDefault();
            //        bool searchable = Convert.ToBoolean(Request.Form.GetValues(col + "[searchable]").FirstOrDefault());

            //        if (searchable)
            //        {
            //            var pi = typeof(ConsentIndexViewModel).GetProperty(colName);

            //            if (pi.PropertyType == typeof(Int32))
            //            {
            //                v = v.Where(colName + "==" + value).ToList();
            //            }
            //            else
            //            {
            //                v = v.Where(colName + " != null && " + colName + ".ToLower().Contains(@0)", value).ToList();
            //            }
            //        }
            //    }
            //}

            // Filter Order by UserLogin
            //if ((UserRole)Session["UserRole"] == UserRole.ContactCenter)
            //{
            //    int iUserID = Convert.ToInt32(Session["UserID"].ToString());
            //    v = v.Where(w => w.I_CREATEDBY == iUserID).ToList();
            //}

            //SORT
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                sortColumn = sortColumn == "S_CREATEDDATE" ? "O_CREATEDDATE" : sortColumn;
                var pi = typeof(ConsentIndexViewModel).GetProperty(sortColumn);

                if (pi != null)
                {
                    if (sortColumnDir == "asc")
                    {
                        v = v.OrderBy(x => pi.GetValue(x, null)).ToList();
                    }
                    else
                    {
                        v = v.OrderByDescending(x => pi.GetValue(x, null)).ToList();
                    }
                }
            }

            recordsTotal = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
        }
    }
}